package assignment;
import java.util.List;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FunctionInterfaceDemo {
    public static void main(String[] args)
    {
        Function<Integer,Double> half=(x)->x/2.0;
        List<Integer> integerList=Arrays.asList(10,39,40,42,44,65,64,33,11,21,37);
        List<Double> listOfHalfValues=integerList.stream().map(x->half.apply(x)).collect(Collectors.toList());
        listOfHalfValues.forEach(System.out::println);

    }
}
