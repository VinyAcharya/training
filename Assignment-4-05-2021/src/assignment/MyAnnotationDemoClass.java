package assignment;

import java.lang.reflect.Method;

public class MyAnnotationDemoClass {
    public static void main(String[] args) throws Exception {
        MyAnnotationDemoClass demo = new MyAnnotationDemoClass();
        Method method = demo.getClass().getMethod("annotationMethod");
        MyAnnotation myAnnotation = method.getAnnotation(MyAnnotation.class);
        System.out.println("Value is :" + myAnnotation.value());

    }

    @MyAnnotation(value = 10)
    public void annotationMethod() {
        System.out.println("My Annotation");
    }
}
